const { AddBookHandler,
getAllBookHandler,
getBookByIdHandler,
editBookHandler, 
deleteBookHandler
} = require('./handler');

const routes = [
    {
        method  :'POST',
        path    : '/books',
        handler : AddBookHandler,
    },
    {
        method  : 'GET',
        path    : '/books',
        handler : getAllBookHandler,
    },
    {
        method  : 'GET',
        path    : '/books/{bookId}',
        handler : getBookByIdHandler,
    },
    {
        methode : 'PUT',
        path    : '/books/{bookId}',
        handler : editBookHandler,
    },
    {
        method  : 'DELETE',
        path    : '/books/{bookId}',
        handler : deleteBookHandler,
    }
]

module.exports =routes;
