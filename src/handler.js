const {nanoid} = require('nanoid');
const books = require('./book');

const AddBookHandler = (req, res) =>{
    const { 
        name, 
        year, 
        author, 
        summary, 
        publisher, 
        pageCount, 
        readPage, 
        reading } = req.payload;

    if(!name){
        const response = req.response({
            status  : "fail",
            message : "Gagal menambahkan buku. Mohon isi nama buku",
        }).code(400);

        return response;
    }

    if(readPage > pageCount){
        const response = req.response({
            status    : "fail",
            message   : "Gagal menambahkan buku. ReadPage tidak boleh lebih besar dari PageCount",
        }).code(400);

        return response;
    }

    const id = nanoid(16);
    const finished = pageCount === readPage;
    const insertAt = new Date().toISOString();
    const updateAt = insertAt;

    const newBooks = {
        name, 
        year, 
        author, 
        summary, 
        publisher, 
        pageCount, 
        readPage, 
        reading, 
        id, 
        finished, 
        insertAt, 
        updateAt,
        
    };

    books.push(newBooks);

   const isSuccess = books.filter((book) => book.id === id).length > 0 ;

    if (isSuccess){
        const response = res.response({
            status    : "success",
            message   : "Buku berhasil ditambahkan",
            data      :{
                bookId : id,
            }
        }).code(201);
        return response
    }else{
        const response = req.response({
            status  : "fail",
            message : "Buku Gagal Ditambahkan"
        }).code(500);
        return response
    }


};

const getAllBookHandler = (req, res) => {
    const{
        name, 
        reading, 
        finished } = req.query;

    if (!name && !reading && !finished){
        const response = res.response({
            status  : "success",
            data    : {
                books : books.map((book) => ({
                    id          : book.id,
                    name        : book.name,
                    publisher   : book.publisher,

                }))
            }
        }).code(200);

        return response;
    }

    if(name){
        const filteredBooksName = books.filter((book) => {
            const nameRegex = new RegExp(name, 'gi');
            return nameRegex.test(book.name);
        });

        const response = res.response({
            status  :   "success",
            data    :{
                books : filteredBooksName.map((book) => ({
                    id  : book.id,
                    name    : book.name,
                    publisher   : book.publisher
                })),
            },
        }).code(200);
        return response;
    }

    if (reading){
        const filterBookReading = books.filter(
            (book) => Number(book.reading) === Number(reading),
        );

        const response = res.resp({
            status  :   "success",
            data    : {
                books   : filterBookReading.map((book) => ({
                    id          : book.id,
                    name        : book.name,
                    publisher   : book.publisher,
                })),
            },
        }).code(200);
        return response;
    }

    const filterBookFinish = books.filter(
        (book) => Number(book.finished) === Number(finished),
    );

    const response = res.response ({
        status  :   "success",
        data    : {
            books : filterBookFinish.map((book) => ({
                id          : book.id,
                name        : book.name,
                publisher   : book.publisher,              
            })),
        },
    }).code(200);
};

const getBookByIdHandler = (req, res) =>{
    const {bookId}  = req.params;

    const book = books.filter((cbook) => cbook.id === bookId)[0];

    if (book){
        const response = res.response ({
            status  : "success",
            data    : {
                book,
            }
        }).code(200);
        return response;
    }

    const response = res.response ({
        status  : "failed",
        message :  "Buku tidak ditemukan"
    }).code(404);
    return response;
};

const   editBookHandler = (req, res) => {
    const {bookId} = req.params;

    const{
        name,
        year,
        author,
        summary,
        publisher,
        pageCount,
        reading,
    } = request.payload;


    if(!name){
        const response = res.response({
          status    : "Failed",
          message   :  "Gagal Memperbaharui buku. Mohon diisi nama buku",  
        }).code(400);
        return response
    }

    if(readPage > pageCount){
        const response = res.response({
            status  :   "failed",
            message :   "Gagal memperbaharui buku. ReadPage tidak boleh lebih besar dari PageCount",
        }).code(400);

        return response
    }

    const finished = pageCount ===readPage;
    const updateAt = new Date().toISOString();

    const indexbook = books.findIndex((cbook) => book.id ===bookId);

    if (indexbook !== -1){
        books[indexbook] = {
            ...books[indexbook],
            name,
            year,
            author,
            summary,
            publisher,
            pageCount,
            readPage,
            reading,
            finished,
            updateAt,
        };

        const response = res.response({
            status  :   "success",
            message :   "Buku berhasil diperbaharui"
        }).code(200);

        return response;
    }

    const response = res.response({
        status  :   "Failed",
        message :   "Gagal memperbaharui buku. Idbook tidak ditemukan",
    }).code(400);
    return response;
};

const deleteBookHandler = (req, res) => {
    const {bookId} = req.params;

    const indexbook = books.filter((cbook)=> book.id === bookId);

    if (indexbook !== -1){
        books.splice(indexbook,1);

        const response = res.response({
            status  :   "success",
            message :   "Buku berhasil dihapus",
        }).code(200);
        return response;
    }

    const response = res.response({
        status  :   "Fail",
        message :   "Buku gagal dihapus. Idbook tidak ditemukan"
    }).code(400);
    return response;
}



module.exports = {
    AddBookHandler,
    getAllBookHandler, 
    getBookByIdHandler, 
    editBookHandler,
    deleteBookHandler
};
